# WakaNoodles + dialog system

<strong>&#10024; Here come the WakaNoodles &#10024;</strong> <br>

[![WakaNoodles](https://img.youtube.com/vi/O7hEPCEm-yw/0.jpg)](https://www.youtube.com/watch?v=O7hEPCEm-yw)

They are cute little noodle animals with tons of animations and changable expressions. There are 9 different models and several textures for each of them which you can change through dropdown lists in Inspector window or via code. You'll get the UV layout image for all of the animals, so you can easily paint your own textures.<br><br>

<strong>&#11088;  But that's not all ... &#11088;</strong><br> The package contains a dialog system, which you can customize for your own creations. <br><br>

<strong>IMPORTANT !!  The dialog system works on any platform, but it was designed for wider screens. </strong> So if you want to use it on mobile platform, it will only functioning with landscape mode. You can see some preview examples among the screenshots.<br><br>

<strong> Package contains: </strong><br>
- 9 different models (1564 faces - 1970 faces) : &#128057; hamster, &#128059; bear, &#128048; rabbit, &#128053; monkey, &#128045; mouse, &#128049; cat, &#129409; lion, &#128054; dog, &#129429; dinosaur <br>
- 20 prefabs (9 noodle types + 9 UI noodle  + 1 manager + 1 dialog system)<br>
- 38 different textures all together <br>
- 37 animations for each animal <br>
- 97 different kawaii expressions (angry, bored, confused, cool, crazy, cry, dead, disappointed, eat, evil, grin, happy, hurt, kiss, laugh, love, sad, sigh, sleep, surprised, tongue, etc...)  <br>
- UV layout reference image for each animal <br>
- 4 different panel types for dialogs (default x 2, think, angry) <br>
- 4 different reveal and hide animations for dialogs (scale, from sides, from bottom) + effects (float, shake)<br>
- demo scene (static forest scene which you can preview in the link below)<br><br>

<strong>Preview demo</strong><br>
https://simmer.io/@PetraHugyecz/wakanoodles

<strong>Unity Asset Store</strong><br>
https://assetstore.unity.com/packages/slug/192035

<strong>Youtube preview</strong><br>
https://www.youtube.com/watch?v=O7hEPCEm-yw

If you have any questions, suggestions or feedback, please feel free to leave a review or contact me at <a href="mailto:petrahugyecz@gmail.com"> petrahugyecz@gmail.com </a> &#9996;
